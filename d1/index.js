/*
	- use the "require" directive to load the express module/package
	- a module is a software componnet or part of a program that contains one or more routines
	- this is used to get the contents of the package to be used by our applictaion
	- it also allows us to access methods and functions that will allow us to easily create a server
*/



const express = require("express");

/*
	- create an application using express
	- this creates an express application and stores this in a constant called app
	- in layman's term, "app" is our server
*/

const app = express()

// for our application server to run we need a port to listen to 
const port = 3000;


// MIDDLEWARES


app.use(express.json())
/*
	- set up for allowing data to handle data from requests
	- allows your app to read json data
	- methods used from express.js are middlewares
	- middleware is a layer of software that enables interaction and trasnmission of information between assorted applications
*/


app.use(express.urlencoded({extended: true}));
/*
	- allows your app to read data from forms
	- by default info received from the URL can only be received as a string or an array
	- by applying the option of "extended: true", we are allowed to receive information in other data types such as an object throughout our application 
*/


// ROUTES
/*
	- express has methods corresponding to each HTTP method
	
	- the full base URL for our local application for this route will be at "http://localhost:3000"
*/

//  RETURN SIMPLE MESSAGE
/* - this route expects to receive a GET request at the base URI
	POSTMAN:
		url: http://localhost:3000/
		method: GET
*/

app.get('/', (request, response) => {
	response.send('Hello World')
});


// RETURNS SIMPLE MESSAGE
/*
	URI: /hello
	Method: 'GET'

	POSTMAN:
	url: http//localhost:3000/hello
	method: GET
*/

app.get('/', (request, response) => {
	response.send('Hello from the "/hello" endpoint')
});




app.listen(port, () => console.log(`Server running at port ${port}`))
/*
	- tells our server to listen to the port
	- if the port is accessed we can run the server
	- returns a message to confirm that the server is running in the terminal
*/

//  RETURNS SIMPLE GREETING
/*
	URI: /helllo
	Method: 'POST'

	POSTMAN
	url: http://localhost:3000/hello
	method: 'POST'
	body: raw + json
	{
		"firstName": "Nehemiah",
		"lastName": "Ellorico" 
	}
*/

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello endpoint but with a post method`)
})

let users = []

// REGISTER USER ROUTE
/*
	- this route expects to receive a POST request at the URI "/register"
	- this will create a user object in the "users" variable that mirrors a real world registration process

	URI: /hello
	method: 'POST'

	POSTMAN:
	url : http://localhost:3000/register
	method: 'POST'
	body: raw + json
	{
		"username": "Gel",
		"password": "1234"
	}
*/

app.post('/register', (request, response) => {
	if (request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please input BOTH username and password')
	}
})


// CHANGE PASSWORD
/*
	- this route expects to receive a PUT request at the URI "/change-password"
	- this will update the password of a user that matches the information provided to change in the client/postman

	URI: /change-password
	method: PUT

	POSTMA: 
	url: http://localhost:3000/change-password
	method: PUT
	body: raw + json
	{
	"username": "Gel",
	"password": "123456"
	}
*/

app.use("/change-password", (request, response) => {
	// creates a variable to store the message to be sent back to the client/postman
	let message;
	// console.log('Works after the message')

	// creates a for loop that will loop through the elements of the "users" array 
	for(let i = 0; i < users.length; i++){

		// if the username provided in the client/postman and the username of the current object in the loop is the same
		if (request.body.username == users[i].username) {

			// changes the password of the user found by the loop into the password provided in the client/postman
			users[i].password = request.body.password

			// changes the message to be sent if the password has been updated
			message = `User ${request.body.username}'s password has been updated`

			// breaks out the loop once a user that matches the username provided in the client/postman is found
				break;

				// if no user is found, else
		}else {
			message = "User does not exist"
		}
	}

	// sends a response back to the client/postman once the password has been updated or if a user has not been found
	response.send(message);
})







